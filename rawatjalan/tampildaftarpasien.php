<?php
require_once("koneksi.php");
require_once("headerpage.php");
?>

<ol class="breadcrumb">
    <li class="breadcrumb-item">
        <a href="index.php">Home</a>
    </li>
    <li class="breadcrumb-item active">Daftar Pasien</li>
</ol>
<div class="row">
  
  <?php 
   $sql = "SELECT no_rm,nm_pasien,tmpt_lahir,tgl_lahir,jns_kelamin,nm_ibu,gol_darah,agama FROM pasien ORDER BY no_rm ASC";  
   $stmt = $conn->prepare($sql);
   $stmt->execute();
   $stmt->bind_result($no_rm,$nm_pasien,$tmpt_lahir,$tgl_lahir,$jns_kelamin,$nm_ibu,$gol_darah,$agama);
  ?>
  <style>
table {
    font-family: arial, sans-serif;
    border-collapse: collapse;
    width: 100%;
  }
  
  td, th {
    border: 1px solid #dddddd;
    text-align: left;
    padding: 8px;
  }
  
</style>
    <table>
<tr>
<th>No RM</th>
<th>Nama Pasien</th>
<th>Tempat Lahir</th>
<th>Tgl Lahir</th>
<th>Jenis Kelamin</th>
<th>Nama Ibu</th>
<th>Gol Darah</th>
<th>Agama</th>
<th>Aksi</th>
</tr>
        <?php
            While($stmt->fetch())
                {
                    Echo "<tr>";
                    Echo "<td>$no_rm</td>";
                    Echo "<td>$nm_pasien</td>";
                    Echo "<td>$tmpt_lahir</td>";
                    Echo "<td>$tgl_lahir</td>";
                    Echo "<td>$jns_kelamin</td>";
                    Echo "<td>$nm_ibu</td>";
                    Echo "<td>$gol_darah</td>";
                    Echo "<td>$agama</td>";
                    echo "<td><a href='tambahtindakan.php?norm=$no_rm'><button type='button' class='btn btn-info'>Tambah Tindakan</button></a> </td>";
                    Echo "</tr>";
                }   
        ?>
       
</table>


<?php 
require_once("footerpage.php");
?>