<?php
require_once("koneksi.php");
require_once("headerpage.php");
?>

<ol class="breadcrumb">
    <li class="breadcrumb-item">
        <a href="index.php">Home</a>
    </li>
    <li class="breadcrumb-item active">Daftar Rawat Jalan</li>
</ol>
<div class="row">
  
  <?php 
   $sql = "SELECT id_tindakan,tindakan,diagnosa,tgl,no_rm FROM tindakan ORDER BY id_tindakan ASC";  
   $stmt = $conn->prepare($sql);
   $stmt->execute();
   $stmt->bind_result($id_tindakan,$tindakan,$diagnosa,$tgl,$no_rm);
  ?>
  <style>
table {
    font-family: arial, sans-serif;
    border-collapse: collapse;
    width: 100%;
  }
  
  td, th {
    border: 1px solid #dddddd;
    text-align: left;
    padding: 8px;
  }
  
</style>
    <table>
<tr>
<th>Id Tindakan</th>
<th>Tindakan</th>
<th>Diagnosa</th>
<th>Tanggal</th>
<th>No RM</th>
<th>Aksi</th>
</tr>
        <?php
            While($stmt->fetch())
                {
                    Echo "<tr>";
                    Echo "<td>$id_tindakan</td>";
                    Echo "<td>$tindakan</td>";
                    Echo "<td>$diagnosa</td>";
                    Echo "<td>$tgl</td>";
                    Echo "<td>$no_rm</td>";
                    echo "<td><a href='tambahantrianfm.php?id_tindakan=$id_tindakan'><button type='button' class='btn btn-info'>Tambah Antrian Farmasi</button></a> </td>";
                    Echo "</tr>";
                }
                
        ?>
</table>

<?php 
require_once("footerpage.php");
?>