<?php

require_once("koneksi.php");
$tindakan = $_POST["tindakan"];
$diagnosa = $_POST["diagnosa"];
$tgl = $_POST["tgl"];
$no_rm = $_POST["no_rm"];


   //$sql="INSERT INTO tindakan(no_rm,tindakan,nm_dokter,cara_bayar,jenis_poli,diagnosa,obat,periksa_penunjang) VALUES ('$no_rm','$tindakan','$nm_dokter','$cara_bayar','$jenis_poli','$diagnosa','$obat','$periksa_penunjang')";
  // echo $sql;
    $stmt = $conn->prepare("INSERT INTO tindakan(tindakan,diagnosa,tgl,no_rm) VALUES (?,?,?,?)");
    $stmt->bind_param("sssi",$tindakan,$diagnosa,$tgl,$no_rm);
    try{
        $stmt->execute();
        //move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file);
        $pesan = "Tindakan berhasil ditambahkan.";
        header("Location: /adminrawatjalan/rawatjalan/tampiltindakan.php?pesan=$pesan");
    }catch(Exception $e)
    {
        $pesan = "Proses tambah tindakan gagal, kesalahan:".$e->getMessage();
        header("Location: /adminrawatjalan/rawatjalan/tambahtindakan.php?pesan=$pesan");
    }finally 
    {
        $stmt->close();
        $conn->close();
    }
//}

?>