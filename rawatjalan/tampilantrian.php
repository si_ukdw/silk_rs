<?php
require_once("koneksi.php");
require_once("headerpage.php");
?>

<ol class="breadcrumb">
    <li class="breadcrumb-item">
        <a href="index.php">Home</a>
    </li>
    <li class="breadcrumb-item active">Daftar Antrian Farmasi</li>
</ol>
<div class="row">
  
  <?php 
   $sql = "SELECT id_tindakan,tgl_obat FROM antrian_fm ORDER BY id_tindakan ASC";  
   $stmt = $conn->prepare($sql);
   $stmt->execute();
   $stmt->bind_result($id_tindakan,$tgl_obat);
  ?>
  <style>
table {
    font-family: arial, sans-serif;
    border-collapse: collapse;
    width: 100%;
  }
  
  td, th {
    border: 1px solid #dddddd;
    text-align: left;
    padding: 8px;
  }
  
</style>
    <table>
<tr>
<th>Id Tindakan</th>
<th>Tanggal Obat</th>
</tr>
        <?php
            While($stmt->fetch())
                {
                    Echo "<tr>";
                    Echo "<td>$id_tindakan</td>";
                    Echo "<td>$tgl_obat</td>";
                    Echo "</tr>";
                }
                
        ?>
</table>

<?php 
require_once("footerpage.php");
?>