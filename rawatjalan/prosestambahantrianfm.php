<?php

require_once("koneksi.php");

$id_tindakan = $_POST["id_tindakan"];
$tgl_obat = $_POST["tgl_obat"];

  // $sql="INSERT INTO antrian_obat(id_antrian,no_rm,no_antrian,tgl_antri) VALUES ('id_antrian','no_rm','no_antrian','tgl_antri')";
  // echo $sql;
    $stmt = $conn->prepare("INSERT INTO antrian_fm(id_tindakan,tgl_obat) VALUES (?,?)");
    $stmt->bind_param("is",$id_tindakan,$tgl_obat);
    try{
        $stmt->execute();
        $pesan = "Antrian berhasil ditambahkan.";
        header("Location: /adminrawatjalan/rawatjalan/tampilantrian.php?pesan=$pesan");
    }catch(Exception $e)
    {
        $pesan = "Proses tambah antrian gagal, kesalahan:".$e->getMessage();
        header("Location: /adminrawatjalan/rawatjalan/tambahantrianfm.php?pesan=$pesan");
    }finally 
    {
        $stmt->close();
        $conn->close();
    }
//}

?>