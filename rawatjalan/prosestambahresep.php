<?php

require_once("koneksi.php");
$id_tindakan = $_POST["id_tindakan"];
$id_obat = $_POST["id_obat"];
$jumlah = $_POST["jumlah"];
$harga = $_POST["harga"];
$total = $_POST["total"];



    $stmt = $conn->prepare("INSERT INTO resep(id_tindakan,id_obat,jumlah,harga,total) VALUES (?,?,?,?,?)");
    $stmt->bind_param("iiiii",$id_tindakan,$id_obat,$jumlah,$harga,$total);
    try{
        $stmt->execute();
       
        $pesan = "Pasien $id_tindakan berhasil ditambahkan.";
        header("Location: adminrawatjalan/rawatjalan/tampilresep.php?pesan=$pesan");
    }catch(Exception $e)
    {
        $pesan = "Proses tambah tindakan gagal, kesalahan:".$e->getMessage();
        header("Location: adminrawatjalan/rawatjalan/tambahresep.php?pesan=$pesan");
    }finally 
    {
        $stmt->close();
        $conn->close();
    }
//}

?>