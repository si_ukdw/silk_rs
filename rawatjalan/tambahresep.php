<?php 
    /*if(!isset($_SESSION["username"])){
        header("Location: /ukdwstore/loginform.php");
    }*/
    require_once("headerpage.php");
?>

<ol class="breadcrumb">
    <li class="breadcrumb-item">
        <a href="index.php">Home</a>
    </li>
    <li class="breadcrumb-item active">Tambah Resep</li>
</ol>
<div class="row">
    <div class="col-6">
        <h1>Tambah Resep</h1>
        <form action="prosestambahresep.php" method="post" enctype="multipart/form-data">
            <div class="form-group">
                <label for="id_resep">ID Tindakan :</label>
                <input type="text" class="form-control" name="id_tindakan">
            </div>
            <div class="form-group">
            <label for="id_obat">ID Obat :</label>
            <input type="text" class="form-control" name="id_obat">
            </div>
            <div class="form-group">
                <label for="jumlah">Jumlah:</label>
                <input type="text" class="form-control" name="jumlah">
            </div>
            <div class="form-group">
                <label for="harga">Harga:</label>
                <input type="text" class="form-control" name="harga">
            </div>
            <div class="form-group">
                <label for="total">Total:</label>
                <input type="text" class="form-control" name="total">
            </div>
            
            <button type="submit" class="btn btn-default">Submit</button>
            </form>
    </div> 
            
<?php 
    require_once("footerpage.php");
?>