<?php 
    require_once("headerpage.php");
?>

<ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="index.php">Home</a>
        </li>
        <li class="breadcrumb-item active">Home Page</li>
</ol>
      <div class="row">
        <div class="col-12">
          <h1>Welcome to Admin Rawat Jalan</h1>
          <h3>Daftar Pasien Baru</h3>
          <style>
table {
    font-family: arial, sans-serif;
    border-collapse: collapse;
    width: 100%;
  }
  
  td, th {
    border: 1px solid #dddddd;
    text-align: left;
    padding: 8px;
  }
  
</style>
          <table>
<tr>
<th>No Rm</th>
<th>Nama Pasien</th>
<th>Id Dokter</th>
<th>No Antrian</th>
<th>Aksi</th>
<tr>
          <?php 
          $jsonurl = "http://localhost/adminrawatjalan/pendaftaran/function/getAntri.php";
          $json = file_get_contents($jsonurl);
          $json_dec = json_decode($json);
          foreach ($json_dec as $data)
          {
            Echo "<tr>";
            Echo "<td>$data->no_rm</td>";
            Echo "<td>$data->nm_pasien</td>";
            Echo "<td>$data->id_dokter</td>";
            Echo "<td>$data->no_antrian</td>";
            echo "<td><a href='tambahtindakan.php?norm=$data->no_rm'><button type='button' class='btn btn-info'>Tambah Tindakan</button></a> </td>";
            Echo "</tr>";
          }
          ?>
</table>

        </div>
      </div>

<?php 
    require_once("footerpage.php");
?>