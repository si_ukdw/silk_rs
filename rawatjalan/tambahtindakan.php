<?php 
    /*if(!isset($_SESSION["username"])){
        header("Location: /ukdwstore/loginform.php");
    }*/
    require_once("headerpage.php");
?>

<ol class="breadcrumb">
    <li class="breadcrumb-item">
    <style> 
textarea {
  width: 100%;
  height: 150px;
  padding: 12px 20px;
  box-sizing: border-box;
  border: 2px solid #ccc;
  border-radius: 4px;
  background-color: #f8f8f8;
  font-size: 16px;
  resize: none;
} 
</style>
        <a href="index.php">Home</a>
    </li>
    <li class="breadcrumb-item active">Tambah Tindakan</li>
</ol>
    <form action="prosestambahtindakan.php" method="post" enctype="multipart/form-data">
            <div class="form-group">
                <label for="tindakan">Tindakan :</label>
                <input type="text" class="form-control" name="tindakan">
            </div>
            <div class="form-group">
                <label for="diagnosa">Diagnosa :</label>
                <input type="text" class="form-control" name="diagnosa">
            </div>
            <div class="form-group">
                <label for="tgl">Tanggal :</label>
                <input type="date" class="form-control" name="tgl">
            </div>
            <div class="form-group">
                <label for="no_rm">No Rm :</label>
                <input type="text" class="form-control" name="no_rm" value="<?php echo $_GET["norm"]; ?>">
            </div>
            <button type="submit" class="btn btn-default">Submit</button>
        </form>
    </div>
</div>



<?php 
    require_once("footerpage.php");
?>