<?php
require_once("bagianatas.php");
require_once("koneksi.php");
?>
            <div id="page-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <h1 class="page-header">DOKTER</h1>
                        </div>
                        <!-- /.col-lg-12 -->
                    </div>
                    <!-- /.row -->
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default">
                                <div class="panel-heading" style="background-color: turquoise;">
                                    DATA DOKTER
                                </div>
                                <!-- /.panel-heading -->
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                            <thead>
                                                <tr>
                                                    <th>ID DOKTER</th>
                                                    <th>NAMA DOKTER</th>
                                                    <th>SIP</th>
                                                    <th>ALAMAT</th>
                                                    <th>AKSI</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                                $sql = $koneksi->query("select * from dokter");
                                                while ($data =$sql->fetch_assoc()) {                                     
                                            ?>
                                                <tr class="odd gradeX">
                                                    <td><?php echo $data['id_dokter'] ?></td>
                                                    <td><?php echo $data['nm_dokter'] ?></td>
                                                    <td><?php echo $data['sip'] ?></td>
                                                    <td><?php echo $data['alamat'] ?></td>
                                                    <td>
                                                        <a onclick="return confirm('Yakin dihapus ?')" href ="hapusdokter.php?id=<?php echo $data['id_dokter'];?>">
                                                        <button type="button" class="btn btn-danger">Hapus</button></a>
                                                        <a href="editdokter.php?id=<?php echo $data['id_dokter']; ?>">
                                                        <button type="button" class="btn btn-info">Ubah</button></a>
                                                    </td>
                                                                                                            
                                                </tr>
                                                <?php } ?>
                                            </tbody>
                                        </table>

                                        <a  href ="tambahdokter.php">
                                                        <button type="button" class="btn btn-primary">Tambah</button></a>
                                    </div>
                                </div>
                                <!-- /.panel-body -->
                            </div>
                            <!-- /.panel -->
                        </div>
                        <!-- /.col-lg-12 -->
                    </div>
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /#page-wrapper -->
<?php
require_once("bagianbawah.php");
?>