<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/obat','ObatController@index');
Route::get('/obat/api','ObatController@show');
Route::get('/obat/{id_obat}/edit','ObatController@edit');
Route::get('/obat/{id_obat}/delete','ObatController@delete');
Route::post('/obat/update','ObatController@update');
Route::post('/obat/create','ObatController@create');
Route::get('/resep/detail/{id_tindakan}','ResepController@detail');
Route::get('/resep/edit/{id_obat}/{id_tindakan}','ResepController@edit');
Route::post('/resep/update','ResepController@update');
Route::get('/resep','ResepController@index');
Route::get('/antrian','AntrianController@index');