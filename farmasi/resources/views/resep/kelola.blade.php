@extends('layouts.master')

@section('alert')
@if(session('sukses'))
    <div class="alert alert-success" role="alert">
    {{session('sukses')}}
    </div>
@endif
@endsection
@section('judul')
<h1 class="h3 mb-0 text-gray-800">Resep</h1>
@endsection
@section('content')
<!-- DataTales Example -->
<div class="col-12">
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Kelola Resep</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table id="dataTable" class="table table-bordered" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>ID Tindakan</th>
                            <th>ID Obat</th>
                            <th>Jumlah</th>
                            <th>Harga</th>
                            <th>Total</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($resep as $resep)
                        <tr>
                            <td>{{$resep->id_tindakan}}</td>
                            <td>{{$resep->id_obat}}</td>
                            <td>{{$resep->jumlah}}</td>
                            <td>{{$resep->harga}}</td>
                            <td>{{$resep->total}}</td>
                            <td><a class="btn btn-warning" href="/resep/edit/{{$resep->id_obat}}/{{$resep->id_tindakan}}" class="btn">Edit</a></td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection