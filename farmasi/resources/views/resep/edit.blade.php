@extends('layouts.master')

@section('head')
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
<link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
@endsection
@section('judul')
<h1 class="h3 mb-0 text-gray-800">Kelola Obat</h1>
@endsection
@section('content')
<!-- Basic Card Example -->
<div class="col-sm-2"></div>
<div class="col-sm-7">
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Edit Obat</h6>
        </div>
        <div class="card-body">
            <form action="/resep/update" method="POST" enctype="multipart/form-data">
                {{csrf_field()}}
                <input name="id_obat" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" value="{{$resep->id_obat}}" hidden>
                <input name="id_tindakan" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" value="{{$resep->id_tindakan}}" hidden>
                <div class="form-group">
                    <label for="exampleInputEmail1">Nama Obat</label>
                    <input name="nama_obat" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" value="{{$obat->nama_obat}}" disabled>
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Jumlah</label>
                    <input name="jumlah" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" value="{{$resep->jumlah}}" >
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Stok Farmasi</label>
                    <input name="stok" type="number" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" value="{{$obat->stok}}" disabled>
                </div>
                <button class="btn btn-primary float-right">Submit</button>
            </form>
        </div>
    </div>
</div>


@endsection