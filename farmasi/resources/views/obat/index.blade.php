@extends('layouts.master')


@section('alert')
@if(session('sukses'))
<div class="alert alert-success" role="alert">
    {{session('sukses')}}
</div>
@endif
@endsection
@section('head')
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
<link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
@endsection
@section('judul')
<h1 class="h3 mb-0 text-gray-800">Obat</h1>
@endsection
@section('content')
<!-- DataTales Example -->
<div class="col-12">
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Data Obat</h6>
        </div>
        <div class="card-body">
            <button type="button" class="btn btn-primary float-right btn-sm" data-toggle="modal" data-target="#exampleModal">
                Tambah Obat
            </button>
            <br><br>
            <div class="table-responsive">
                <table id="dataTable" class="table table-bordered" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>Nama Obat</th>
                            <th>Golongan</th>
                            <th>Harga</th>
                            <th>Stok</th>
                            <th>Expired</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($obat as $data)
                        <tr>
                            <td>{{$data->nama_obat}}</td>
                            <td>{{$data->golongan}}</td>
                            <td>{{$data->harga}}</td>
                            <td>{{$data->stok}}</td>
                            <td>{{$data->expired}}</td>
                            <td>
                                <a href="/obat/{{$data->id_obat}}/edit"><button class="btn btn-warning">Edit</button></a>
                                <a href="/obat/{{$data->id_obat}}/delete"><button class="btn btn-danger">Delete</button></a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>


<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Tambah Obat</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="/obat/create" method="POST">
                    {{csrf_field()}}
                    <div class="form-group">
                        <label for="exampleInputPassword1">Nama Obat</label>
                        <input name="nama_obat" type="text" class="form-control" id="exampleInputPassword1" placeholder="Enter Nama">
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlSelect1">Golongan</label>
                        <select name="golongan" class="form-control" id="exampleFormControlSelect1">
                            <option value="1">1</option>
                            <option value="2">2</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Harga</label>
                        <input name="harga" type="text" class="form-control" id="exampleInputPassword1" placeholder="Harga">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Stok</label>
                        <input name="stok" type="number" class="form-control" id="exampleInputPassword1" placeholder="Stok">
                    </div>
                    <div class="form-group">
                        <label for="datepicker">Tanggal Expired</label>
                        <input name="expired" id="datepicker1" width="276" />
                        <script>
                            $('#datepicker1').datepicker({
                                uiLibrary: 'bootstrap4'
                            });
                        </script>
                    </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
            </form>
        </div>
    </div>
</div>
@endsection