@extends('layouts.master')


@section('alert')
@if(session('sukses'))
<div class="alert alert-success" role="alert">
    {{session('sukses')}}
</div>
@endif
@endsection
@section('judul')
<h1 class="h3 mb-0 text-gray-800">Antrian</h1>
@endsection
@section('content')
<!-- DataTales Example -->
<div class="col-12">
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Data Antrian</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table id="dataTable" class="table table-bordered" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>ID Tindakan</th>
                            <th>Tanggal Obat</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($antrian as $data)
                        <tr>
                            <td>{{$data->id_tindakan}}</td>
                            <td>{{$data->tgl_obat}}</td>
                            
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>


    </div>
</div>
@endsection