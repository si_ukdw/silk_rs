<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Antrian extends Model
{
    protected $table = 'Antrian_FM';
    protected $fillable = ['id_tindakan','tgl_obat'];
}
