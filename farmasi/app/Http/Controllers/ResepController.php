<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ResepController extends Controller
{
    public function index()
    {
        /*$jsonurl = "https://jsonplaceholder.typicode.com/todos";
        $json = file_get_contents($jsonurl);
        $json_dec = json_decode($json);*/
        $resep = \App\Resep::groupBy('id_tindakan')->select('id_tindakan', DB::raw('count(*) as total'))->get();        
        return view('resep.index',['resep' => $resep]);
    }

    public function detail($id_tindakan)
    {
        $resep = \App\Resep::where('id_tindakan',$id_tindakan)->get();
        return view('resep.kelola',['resep' => $resep]);
    }

    public function edit($id_obat,$id_tindakan)
    {
        
        $resep = \App\Resep::where('id_tindakan',$id_tindakan)->where('id_obat',$id_obat)->first();
        $obat = \App\Obat::find($id_obat);
        //dd($resep);
        return view('resep.edit',['resep' => $resep , 'obat' => $obat]);
    }

    public function update(Request $request)
    {
       
        $resep = \App\Resep::where('id_tindakan',$request->id_tindakan)->where('id_obat',$request->id_obat)->first();
        
        $resep->jumlah = $request->jumlah;
        $resep->save();
        
        return redirect('/resep/detail/'.$request->id_tindakan)->with('sukses','Data Resep Berhasil Diupdated !');
    }
}
