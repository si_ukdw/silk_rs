<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Response;

class ObatController extends Controller
{
    public function index()
    {
        $obat = \App\Obat::all();
        return view('obat.index',['obat' => $obat]);
    }

    public function create(Request $request)
    {
        $obat = \App\Obat::create($request->all());
        $request->merge(['expired' => date('Y-m-d', strtotime($request->expired))]);
        return redirect('/obat')->with('Data Obat Berhasil Ditambahkan !');
    }

    public function show()
    {
        $obat = \App\Obat::all();
        return Response::json($obat,200);
    }

    public function edit($id_obat)
    {
        $obat = \App\Obat::find($id_obat);
        return view('obat.edit',['obat' => $obat]);
    }

    public function update(Request $request)
    {
        $obat = \App\Obat::find($request->id_obat);
        $request->merge(['expired' => date('Y-m-d', strtotime($request->expired))]);
        $obat->update($request->all());
        return redirect('/obat')->with('sukses','Data Obat Berhasil Diupdated !');
    }

    public function delete($id_obat)
    {
        $obat = \App\Obat::find($id_obat);
        $obat->delete();
        return redirect('/obat')->with('sukses','Data Obat Berhasil Dihapus!');
    }
}
