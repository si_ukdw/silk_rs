<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AntrianController extends Controller
{
    public function index()
    {
        $antrian = \App\Antrian::all();
        return view('antrian.index',['antrian' => $antrian]);
    }
}
