<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Obat extends Model
{
    protected $table = 'Obat';
    protected $primaryKey = 'id_obat';
    protected $fillable = ['nama_obat','golongan','harga','stok','expired'];
}
