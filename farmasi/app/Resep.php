<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Resep extends Model
{
   protected $table = 'Resep';
   protected $primaryKey = 'id_tindakan';
   protected $fillable = ['id_tindakan','id_obat','jumlah','harga','total'];
}
